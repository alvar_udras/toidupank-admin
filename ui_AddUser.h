/********************************************************************************
** Form generated from reading UI file 'AddUser.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDUSER_H
#define UI_ADDUSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddUser
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QLabel *usernameLabel;
    QLineEdit *usernameEdit;
    QLineEdit *lastNameEdit;
    QLabel *pswdLabel;
    QLabel *lastnameLabel;
    QSpacerItem *verticalSpacer;
    QLineEdit *pswdEdit;
    QLineEdit *foreNameEdit;
    QLabel *forenameLabel;
    QLineEdit *emailEdit;
    QComboBox *receivesEmailsCombo;
    QLabel *emailLabel;
    QLabel *receivesEmailsLabel;
    QListView *areaList;

    void setupUi(QDialog *AddUser)
    {
        if (AddUser->objectName().isEmpty())
            AddUser->setObjectName(QStringLiteral("AddUser"));
        AddUser->resize(537, 393);
        buttonBox = new QDialogButtonBox(AddUser);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(170, 350, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        horizontalLayoutWidget_2 = new QWidget(AddUser);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 14, 501, 331));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        usernameLabel = new QLabel(horizontalLayoutWidget_2);
        usernameLabel->setObjectName(QStringLiteral("usernameLabel"));

        gridLayout->addWidget(usernameLabel, 0, 0, 1, 1);

        usernameEdit = new QLineEdit(horizontalLayoutWidget_2);
        usernameEdit->setObjectName(QStringLiteral("usernameEdit"));

        gridLayout->addWidget(usernameEdit, 0, 2, 1, 1);

        lastNameEdit = new QLineEdit(horizontalLayoutWidget_2);
        lastNameEdit->setObjectName(QStringLiteral("lastNameEdit"));

        gridLayout->addWidget(lastNameEdit, 2, 2, 1, 1);

        pswdLabel = new QLabel(horizontalLayoutWidget_2);
        pswdLabel->setObjectName(QStringLiteral("pswdLabel"));

        gridLayout->addWidget(pswdLabel, 5, 0, 1, 1);

        lastnameLabel = new QLabel(horizontalLayoutWidget_2);
        lastnameLabel->setObjectName(QStringLiteral("lastnameLabel"));

        gridLayout->addWidget(lastnameLabel, 2, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 9, 2, 1, 1);

        pswdEdit = new QLineEdit(horizontalLayoutWidget_2);
        pswdEdit->setObjectName(QStringLiteral("pswdEdit"));

        gridLayout->addWidget(pswdEdit, 5, 2, 1, 1);

        foreNameEdit = new QLineEdit(horizontalLayoutWidget_2);
        foreNameEdit->setObjectName(QStringLiteral("foreNameEdit"));

        gridLayout->addWidget(foreNameEdit, 1, 2, 1, 1);

        forenameLabel = new QLabel(horizontalLayoutWidget_2);
        forenameLabel->setObjectName(QStringLiteral("forenameLabel"));

        gridLayout->addWidget(forenameLabel, 1, 0, 1, 1);

        emailEdit = new QLineEdit(horizontalLayoutWidget_2);
        emailEdit->setObjectName(QStringLiteral("emailEdit"));

        gridLayout->addWidget(emailEdit, 3, 2, 1, 1);

        receivesEmailsCombo = new QComboBox(horizontalLayoutWidget_2);
        receivesEmailsCombo->setObjectName(QStringLiteral("receivesEmailsCombo"));

        gridLayout->addWidget(receivesEmailsCombo, 4, 2, 1, 1);

        emailLabel = new QLabel(horizontalLayoutWidget_2);
        emailLabel->setObjectName(QStringLiteral("emailLabel"));

        gridLayout->addWidget(emailLabel, 3, 0, 1, 1);

        receivesEmailsLabel = new QLabel(horizontalLayoutWidget_2);
        receivesEmailsLabel->setObjectName(QStringLiteral("receivesEmailsLabel"));

        gridLayout->addWidget(receivesEmailsLabel, 4, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout);

        areaList = new QListView(horizontalLayoutWidget_2);
        areaList->setObjectName(QStringLiteral("areaList"));

        horizontalLayout->addWidget(areaList);


        retranslateUi(AddUser);
        QObject::connect(buttonBox, SIGNAL(accepted()), AddUser, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AddUser, SLOT(reject()));

        QMetaObject::connectSlotsByName(AddUser);
    } // setupUi

    void retranslateUi(QDialog *AddUser)
    {
        AddUser->setWindowTitle(QApplication::translate("AddUser", "Dialog", 0));
        usernameLabel->setText(QApplication::translate("AddUser", "Kasutajanimi", 0));
        pswdLabel->setText(QApplication::translate("AddUser", "Parool", 0));
        lastnameLabel->setText(QApplication::translate("AddUser", "Perenimi", 0));
        forenameLabel->setText(QApplication::translate("AddUser", "Eesnimi", 0));
        receivesEmailsCombo->clear();
        receivesEmailsCombo->insertItems(0, QStringList()
         << QApplication::translate("AddUser", "Jah", 0)
         << QApplication::translate("AddUser", "Ei", 0)
        );
        emailLabel->setText(QApplication::translate("AddUser", "e-mail", 0));
        receivesEmailsLabel->setText(QApplication::translate("AddUser", "saab emaile", 0));
    } // retranslateUi

};

namespace Ui {
    class AddUser: public Ui_AddUser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDUSER_H
