#pragma once
#include <QAbstractListModel>
#include "Piirkond.h"

struct PiirkondWithCheckedBool
{
	PiirkondWithCheckedBool ( Piirkond& p, bool c ) : piirkond(p), checked(c ) {}
	Piirkond piirkond;
	bool checked;
};

class PiirkondListModel : public QAbstractListModel
{
	Q_OBJECT
public:
    PiirkondListModel( QList<Piirkond>& piirkonnad, QObject* p = 0 );
	int rowCount ( const QModelIndex & parent = QModelIndex() ) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	Qt::ItemFlags flags(const QModelIndex& index) const override;	
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
protected:
	QList<PiirkondWithCheckedBool> m_piirkonnad;
};

class PiirkondListModelWithCheckbox : public PiirkondListModel
{
    Q_OBJECT
public:
    PiirkondListModelWithCheckbox ( QList<Piirkond>& piirkonnad, QObject* p = 0 );
    QList<Piirkond> getChecked();
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void setChecked( Piirkond& piirkond );
};
