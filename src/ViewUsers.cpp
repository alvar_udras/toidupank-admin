#include "ViewUsers.h"
#include "ui_ViewUsers.h"
#include "User.h"

ViewUsers::ViewUsers(IRepositoryFactory& repoFactory, QWidget *parent) :
    QDialog(parent),
    m_repoFactory(repoFactory),
    ui(new Ui::ViewUsers)
{
    ui->setupUi(this);
    connect( ui->pushButton, SIGNAL(clicked()), this, SLOT(editUserPressed()));
}

ViewUsers::~ViewUsers()
{
    delete ui;
}

void ViewUsers::setUserListModel(QAbstractListModel* userListModel)
{
    ui->userListView->setModel(userListModel);
    QObject::connect( ui->userListView->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), this,  SLOT( userSelected(QModelIndex))  );
}

void ViewUsers::setPiirkondListModel(QAbstractListModel *piirkondListModel)
{
    ui->userAreasList->setModel(piirkondListModel);
}

void ViewUsers::userSelected(const QModelIndex &index)
{
    User u = m_userListModel->QModelIndexToUser(index);
    m_piirkndListModel.reset( new PiirkondListModel( u.piirkonnad ) );
    setPiirkondListModel( m_piirkndListModel.data() );
}

void ViewUsers::editUserPressed()
{
    User u = m_userListModel->QModelIndexToUser( ui->userListView->currentIndex());
    emit changeUserPressedSignal(u);
}

void ViewUsers::showModalDialog()
{
    QList<User> users = m_repoFactory.getUserRepository()->getAll();
    m_userListModel.reset( new UserListModel(users, this) );
    setModal(true);
    setUserListModel(m_userListModel.data());
    exec();
}


