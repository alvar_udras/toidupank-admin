#ifndef PIIRKONDDUMMYREPOSITORY_H
#define PIIRKONDDUMMYREPOSITORY_H

#include "PiirkondRepository.h"

class PiirkondDummyRepository : public IPiirkondRepository
{
public:
    QList<Piirkond> getAll() override;
    static QList<Piirkond> m_piirkonnad;
};

#endif // PIIRKONDDUMMYREPOSITORY_H
