#include <QApplication>
#include "SqlManager.h"
#include "AppController.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	SqlManager::init();

    AppController appConroller;
    
    return a.exec();
}
