#ifndef USERDUMMYREPOSITORY_H
#define USERDUMMYREPOSITORY_H

#include "UserRepository.h"

class UserDummyRepository : public IUserRepository
{
public:
    UserDummyRepository();
    void Save( User& user ) override
    {
        m_userList.append(user);
        qDebug() <<  "Saving user";
    }

    QList<User> getAll() override;
private:
    static QList<User> m_userList;
};


#endif // USERDUMMYREPOSITORY_H
