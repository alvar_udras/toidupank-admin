#include "UserDummyRepository.h"
#include "PiirkondDummyRepository.h"

QList<User> UserDummyRepository::m_userList = QList<User>();

UserDummyRepository::UserDummyRepository()
{
}


QList<User> UserDummyRepository::getAll()
{
    PiirkondDummyRepository piirkondDummyRepo;
    QList<Piirkond> piirkonnad = piirkondDummyRepo.getAll();
    if ( m_userList.size() == 0 )
    {
        QList<Piirkond> piirkonnadTemp;

        piirkonnadTemp.append(piirkonnad[1]);
        piirkonnadTemp.append(piirkonnad[2]);

        User u;
        u.eesnimi = "Alvar";
        u.parool = "parool";
        u.email = "alvar.udras@gmail.com";
        u.id = 1;
        u.kasutajanimi = "alvarudras";
        u.perenimi = "Udras";
        u.piirkonnad = piirkonnadTemp;
        m_userList.append(u);

        piirkonnadTemp.clear();
        piirkonnadTemp.append(piirkonnad[3]);
        piirkonnadTemp.append(piirkonnad[4]);
        piirkonnadTemp.append(piirkonnad[5]);

        u.eesnimi = "Viktor";
        u.parool = "Kana";
        u.email = "viktor.kana@gmail.com";
        u.id = 2;
        u.kasutajanimi = "viktorkana";
        u.perenimi = "Kana";
        u.piirkonnad = piirkonnadTemp;

        m_userList.append( u );
    }
    return m_userList;
}
