#include "UserRepository.h"
#include "Queries.h"
#include <QSqlQuery>

void UserDbRepository::Save(User& user)
{
	QSqlQuery qry;
	qry.prepare( Queries::saveUser );
	qry.bindValue( 0, user.eesnimi );
	qry.bindValue( 1, user.perenimi );
	qry.bindValue( 2, user.kasutajanimi );
	qry.bindValue( 3, user.email );
	qry.bindValue( 4, user.receivesEmails );
	qry.bindValue( 5, user.parool );
	qry.exec();

	if ( user.id == -1 )
	{
		user.id = qry.lastInsertId().toInt();
	}

	qry.prepare( Queries::savePiirkondUser );

	for ( int i = 0 ; i < user.piirkonnad.count() ; i++ )
	{
		qry.bindValue( 0, user.piirkonnad[i].getId() );
		qry.bindValue( 1, user.id );
		qry.exec();
	}
}
