#pragma once
#include <QObject>
#include "adduserdialog.h"
#include "Piirkond.h"

class User
{
public:
    User(void) : id(-1){};
	virtual ~User(void){};
	int id;
	QString eesnimi;
	QString perenimi;
	QString kasutajanimi;
	QString parool;
	QString email;
	bool receivesEmails;
	QList<Piirkond> piirkonnad;
};
