#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <QObject>
#include "mainwindow.h"

class AppController : public QObject
{
    Q_OBJECT
public:
    explicit AppController(QObject *parent = 0);

signals:

public slots:
    void displayAddUser(User& u);
    void displayAddUser();
    void displayUsers();
    void displayPrefilledUser();
    void editUser( User u );

private:
    QScopedPointer<MainWindow> m_mainWnd;
    QScopedPointer<IRepositoryFactory> m_repoFactory;
};

#endif // APPCONTROLLER_H
