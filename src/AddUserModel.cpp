#pragma once
#include "AddUserModel.h"
#include <QSharedPointer>
#include <QCryptographicHash>

void AddUserModel::exec()
{
    QList<Piirkond> piirkonnad = m_repoFactory.getPiirkondRepository()->getAll();
    m_piirkondListModel = QSharedPointer<PiirkondListModelWithCheckbox>(new  PiirkondListModelWithCheckbox(piirkonnad) );

    std::for_each( m_user.piirkonnad.begin()
                       , m_user.piirkonnad.end(), [this] ( Piirkond& p) { m_piirkondListModel->setChecked(p);} );

    m_ui.setListModel( *m_piirkondListModel );
    m_ui.setUserName( m_user.kasutajanimi);
    m_ui.setForeName( m_user.eesnimi);
    m_ui.setLastName(m_user.perenimi);
    m_ui.setEmail(m_user.email);
    m_ui.setPassword(m_user.parool);
    m_ui.display();


}

void AddUserModel::onSubmit()
{
    QString foreName = m_ui.getForeName();
    QString lastName = m_ui.getLastName();
    QString password = m_ui.getPassword();
    QString userName = m_ui.getUserName();
    QString passwordHash = QString(QCryptographicHash::hash(password.toLocal8Bit(), QCryptographicHash::Md5));
    QList<Piirkond> selectedAreas = m_piirkondListModel->getChecked();
    m_user.eesnimi = foreName;
    m_user.perenimi = lastName;
	m_user.parool = passwordHash;
    m_user.piirkonnad = selectedAreas;
	m_user.kasutajanimi = userName;
    m_user.email = m_ui.getEmail();
    m_user.receivesEmails = m_ui.receivesEmails();
    m_repoFactory.getUserRepository()->Save(m_user);
}

