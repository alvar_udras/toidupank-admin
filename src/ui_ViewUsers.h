/********************************************************************************
** Form generated from reading UI file 'ViewUsers.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWUSERS_H
#define UI_VIEWUSERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_ViewUsers
{
public:
    QListView *userListView;
    QListView *userAreasList;
    QPushButton *pushButton;

    void setupUi(QDialog *ViewUsers)
    {
        if (ViewUsers->objectName().isEmpty())
            ViewUsers->setObjectName(QStringLiteral("ViewUsers"));
        ViewUsers->resize(616, 300);
        userListView = new QListView(ViewUsers);
        userListView->setObjectName(QStringLiteral("userListView"));
        userListView->setGeometry(QRect(10, 30, 256, 192));
        userAreasList = new QListView(ViewUsers);
        userAreasList->setObjectName(QStringLiteral("userAreasList"));
        userAreasList->setGeometry(QRect(310, 30, 256, 192));
        pushButton = new QPushButton(ViewUsers);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(30, 250, 101, 23));

        retranslateUi(ViewUsers);

        QMetaObject::connectSlotsByName(ViewUsers);
    } // setupUi

    void retranslateUi(QDialog *ViewUsers)
    {
        ViewUsers->setWindowTitle(QApplication::translate("ViewUsers", "Dialog", 0));
        pushButton->setText(QApplication::translate("ViewUsers", "Muuda kasutajat", 0));
    } // retranslateUi

};

namespace Ui {
    class ViewUsers: public Ui_ViewUsers {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWUSERS_H
