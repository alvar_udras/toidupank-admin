#pragma once
#include "PiirkondListModel.h"


PiirkondListModel::PiirkondListModel( QList<Piirkond>& piirkonnad, QObject* p ) : QAbstractListModel ( p )
{
	for ( int i = 0 ; i < piirkonnad.length() ; i++ )
	{
		m_piirkonnad.append( PiirkondWithCheckedBool( piirkonnad[i], false ) );
	}
}

int PiirkondListModel::rowCount ( const QModelIndex & parent ) const
{
	return m_piirkonnad.count();
}
QVariant PiirkondListModel::data(const QModelIndex &index, int role ) const
{
	if ( role == Qt::DisplayRole )
		return m_piirkonnad[index.row()].piirkond.getName();
	return QVariant();
}

Qt::ItemFlags PiirkondListModel::flags(const QModelIndex& index) const
{
	return QAbstractListModel::flags(index) | Qt::ItemIsUserCheckable;
}

bool PiirkondListModel::setData(const QModelIndex &index, const QVariant &value, int role ) 
{
	if ( role == Qt::CheckStateRole )
	{
		m_piirkonnad[index.row()].checked = (value == Qt::Checked);
		return true;
	}
	return QAbstractListModel::setData( index, value, role );
}


PiirkondListModelWithCheckbox::PiirkondListModelWithCheckbox(QList<Piirkond> &piirkonnad, QObject* p) : PiirkondListModel ( piirkonnad, p )
{
}


QList<Piirkond> PiirkondListModelWithCheckbox::getChecked()
{
    QList<Piirkond> res;
    for ( int i = 0 ; i < m_piirkonnad.length() ; i++ )
    {
        if ( m_piirkonnad[i].checked )
            res.append( m_piirkonnad[i].piirkond );
    }
    return res;
}

QVariant PiirkondListModelWithCheckbox::data(const QModelIndex &index, int role ) const
{
    if ( role == Qt::CheckStateRole )
    {
        if ( m_piirkonnad[index.row()].checked )
            return Qt::Checked;
        return Qt::Unchecked;
    }
    else
        return PiirkondListModel::data(index, role);
}

void PiirkondListModelWithCheckbox::setChecked(Piirkond &piirkond)
{
    std::for_each( m_piirkonnad.begin(), m_piirkonnad.end(), [this, &piirkond] (PiirkondWithCheckedBool& p)
    {
        if ( p.piirkond.getName() == piirkond.getName())
            p.checked = true;
    });
}
