#include "PiirkondDummyRepository.h"

QList<Piirkond>  PiirkondDummyRepository::m_piirkonnad = QList<Piirkond>();

QList<Piirkond> PiirkondDummyRepository::getAll()
{
    if ( m_piirkonnad.size() == 0 )
    {
        QList<Piirkond> res;
        QString laanemaa = "Laanemaa";
        Piirkond p( laanemaa, 1 );
        m_piirkonnad.append( p );
        QString harjumaa = "Harjumaa";
        Piirkond p2 ( harjumaa , 2 );
        m_piirkonnad.append( p2 );
        QString polvamaa = "Põlvamaa";
        Piirkond p3 ( polvamaa , 3 );
        m_piirkonnad.append( p3 );
        QString raplamaa = "Raplamaa";
        Piirkond p4 ( raplamaa , 4 );
        m_piirkonnad.append( p4 );
        QString vorumaa = "Võrumaa";
        Piirkond p5 ( vorumaa, 5 );
        m_piirkonnad.append( p5 );
        QString parnumaa = "Pärnumaa";
        Piirkond p6 ( parnumaa, 6 );
        m_piirkonnad.append( p6 );
    }
    return m_piirkonnad;
}
