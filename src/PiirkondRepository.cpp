#include "PiirkondRepository.h"
#include <QSqlQuery>
#include "Queries.h"
#include <QList>
#include <QVariant>

const QString PiirkondDbRepository::nameField = "nimi";
const QString PiirkondDbRepository::idField = "id";

QList<Piirkond>  PiirkondDbRepository::getAll()
{
	QList<Piirkond> piirkonnad;

	QSqlQuery qry;
	qry.exec( Queries::allPiirkonnad );

	while ( qry.next() ) 
	{
		piirkonnad.append( Piirkond( qry.value(1).toString(), qry.value(0).toInt() ));
	}
	return piirkonnad;
}
