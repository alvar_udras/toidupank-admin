#pragma once
#include <QSharedPointer>
#include <QAbstractListModel>
#include "IAddUserModel.h"

class IAddUserUI
{
public:
    virtual void setListModel ( QAbstractListModel& model ) = 0;
    virtual void addModel( QSharedPointer<IAddUserModel> ctrl ) = 0;
	virtual void display() = 0;
    virtual QString getForeName() = 0;
    virtual QString getLastName() = 0;
    virtual QString getPassword() = 0;
    virtual QString getUserName() = 0;
	virtual QString getEmail() = 0;
    virtual void setForeName( const QString& forename ) = 0;
    virtual void setLastName( const QString& lastName ) = 0;
    virtual void setPassword( const QString& password ) = 0;
    virtual void setUserName( const QString& userName ) = 0;
    virtual void setEmail( const QString& email ) = 0;
    virtual void setReceivesEmails(const bool receivesEmails ) = 0;
	virtual bool receivesEmails() = 0;
};
