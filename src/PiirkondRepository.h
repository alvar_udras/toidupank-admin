#pragma once
#include <QString>
#include <QList>
#include "Piirkond.h"

class IPiirkondRepository
{
public:
	virtual QList<Piirkond> getAll() = 0;
};

class PiirkondDbRepository : public IPiirkondRepository
{
public:
	const static QString nameField;
	const static QString idField;
	QList<Piirkond> getAll() override;
};


