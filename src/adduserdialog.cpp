#include "adduserdialog.h"

AddUserDialog::AddUserDialog(QWidget *parent) :
    QDialog(parent)
{
    ui.setupUi(this);
    qDebug()<<"Add user dialog created";
}

void AddUserDialog::setListModel( QAbstractListModel& model ) 
{
    ui.areaList->setModel(&model);
}

void AddUserDialog::addModel( QSharedPointer<IAddUserModel> ctrl )
{
    m_ctrl = ctrl;
    QObject::connect( ui.buttonBox, SIGNAL(accepted()), this,  SLOT( onDataSubmit() ) );
}

void AddUserDialog::onDataSubmit()
{
    if ( m_ctrl )
        m_ctrl->onSubmit();
}

void AddUserDialog::display()
{
	setModal(true);
	exec();
}
