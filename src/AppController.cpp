#include "AppController.h"
#include "adduserdialog.h"
#include "AddUserModel.h"
#include "PiirkondListModel.h"
#include "RepositoryFactory.h"
#include "UserListModel.h"
#include "ViewUsers.h"
#include "User.h"


AppController::AppController(QObject *parent) :
    QObject(parent)
{
    m_mainWnd.reset(new MainWindow());
    m_mainWnd->show();
    m_repoFactory.reset(new DummyRepositoryFactory() );

    connect(m_mainWnd.data(), SIGNAL(addUserRequested()), this, SLOT(displayAddUser()));
    connect(m_mainWnd.data(), SIGNAL(showUsersRequested()), this, SLOT(displayUsers()));
    connect(m_mainWnd.data(), SIGNAL(addUserPrefilledRequested()), this, SLOT(displayPrefilledUser()) );
}

void AppController::displayAddUser(User& u)
{
    QScopedPointer<AddUserDialog> addUserDialog (new AddUserDialog(m_mainWnd.data()) );
    QSharedPointer<AddUserModel> addUserModel(new AddUserModel(*m_repoFactory, *addUserDialog, u));
    addUserDialog->addModel( addUserModel );
    addUserModel->exec();
}

void AppController::displayAddUser()
{
    User u;
    displayAddUser(u);
}


void AppController::displayUsers()
{
    ViewUsers viewUsers(*m_repoFactory, m_mainWnd.data());
    connect(&viewUsers, SIGNAL(changeUserPressedSignal(User)), this, SLOT(editUser(User)));
    viewUsers.showModalDialog();
}

void AppController::displayPrefilledUser()
{
    User testUser;
    testUser.kasutajanimi = "alvarudras";
    testUser.eesnimi = "Eesnimi";
    testUser.perenimi = "Perenimi";
    testUser.parool = "kana";
    testUser.email = "alvar.udras@gmail.com";
    QList<Piirkond> piirkonnad = m_repoFactory->getPiirkondRepository()->getAll();
    QList<Piirkond> tempPiirkonnad;
    tempPiirkonnad.append(piirkonnad[1]);
    tempPiirkonnad.append(piirkonnad[2]);
    testUser.piirkonnad = tempPiirkonnad;
    displayAddUser(testUser);
}

void AppController::editUser(User u)
{
    displayAddUser(u);
}
