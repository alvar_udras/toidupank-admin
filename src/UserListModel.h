#ifndef USERLISTMODEL_H
#define USERLISTMODEL_H

#include <QAbstractListModel>
#include "User.h"

class UserListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit UserListModel(QList<User> users, QObject *parent = 0);

    int rowCount ( const QModelIndex &parent = QModelIndex() ) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    User QModelIndexToUser( const QModelIndex& index );

    User selectedUser();

signals:

public slots:
private:
    QList<User> m_users;
};

#endif // USERLISTMODEL_H
