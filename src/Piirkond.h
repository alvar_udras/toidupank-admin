#pragma once
#include <QString>

class Piirkond
{
public:
	Piirkond( QString& name, int id ) : m_name(name), m_id(id) {}
	QString getName() const { return m_name; };
    int getId() { return m_id; }
private:
	int m_id;
	QString m_name;
};
