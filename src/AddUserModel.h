#pragma once
#include <QAbstractListModel>
#include "IAddUserUI.h"
#include "IAddUserModel.h"
#include "Piirkond.h"
#include "PiirkondListModel.h"
#include <QSharedPointer>
#include "RepositoryFactory.h"
#include "User.h"

class AddUserModel : public IAddUserModel
{
public:
    AddUserModel ( IRepositoryFactory& repoFactory, IAddUserUI& ui, User& u ) : m_repoFactory ( repoFactory ), m_ui(ui), m_user(u) {}
    void exec();
    void onSubmit() override;
private:
    IAddUserUI& m_ui;
    QSharedPointer<PiirkondListModelWithCheckbox> m_piirkondListModel;
    User& m_user;
    IRepositoryFactory& m_repoFactory;
};
