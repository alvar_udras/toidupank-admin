#pragma once
#include <QObject>
#include <QDebug>
#include "adduserdialog.h"
#include "Piirkond.h"
#include "User.h"

class IUserRepository
{
public:
	virtual void Save( User& user ) = 0;
    virtual QList<User> getAll() = 0;
};

class UserDbRepository : public IUserRepository
{
public:
    void Save( User& user ) override;
    QList<User> getAll() override { return QList<User>();}
};
