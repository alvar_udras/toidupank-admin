#ifndef VIEWUSERCONTROLLER_H
#define VIEWUSERCONTROLLER_H

#include "UserListModel.h"
#include "PiirkondListModel.h"
#include "RepositoryFactory.h"


class ViewUserController{

public:
    ViewUserController( QWidget* uiParent );

private:
    QList<User> m_users;


};


#endif // VIEWUSERCONTROLLER_H
