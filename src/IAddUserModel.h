#pragma once

class IAddUserModel
{
public:
    virtual void onSubmit() = 0;
};
