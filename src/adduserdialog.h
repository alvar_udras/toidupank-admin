#ifndef ADDUSERDIALOG_H
#define ADDUSERDIALOG_H

#include <QDialog>
#include <QDebug>
#include "ui_AddUser.h"
#include "IAddUserUI.h"

class AddUserDialog : public QDialog, public IAddUserUI
{
    Q_OBJECT
public:
    explicit AddUserDialog(QWidget *parent = 0);
    ~AddUserDialog() override {qDebug()<<"Add user dialog destroyed";}
    void setListModel( QAbstractListModel& model ) override;
    void addModel( QSharedPointer<IAddUserModel> ctrl ) override;
	void display() override;
    QString getForeName() override { return ui.foreNameEdit->text(); }
    QString getLastName() override { return ui.lastNameEdit->text(); }
    QString getPassword() override { return ui.pswdEdit->text(); }
    QString getUserName() override { return ui.usernameEdit->text(); }
	QString getEmail() override { return ui.emailEdit->text(); }
	bool receivesEmails() override { return ui.receivesEmailsCombo->currentText().toLower() == "jah" ? true : false; }

    void setForeName(const QString &forename)
    {
        ui.foreNameEdit->setText(forename);
    }
    void setLastName(const QString &lastName)
    {
        ui.lastNameEdit->setText(lastName);
    }
    void setPassword(const QString &password)
    {
        ui.pswdEdit->setText(password);
    }
    void setUserName(const QString &userName)
    {
        ui.usernameEdit->setText(userName);
    }
    void setEmail(const QString &email)
    {
        ui.emailEdit->setText(email);
    }
    void setReceivesEmails(const bool receivesEmails)
    {
//        ui.receivesEmailsCombo->set
    }

signals:
    
private slots:
    void onDataSubmit();

private:
    Ui::AddUser ui;
    QSharedPointer<IAddUserModel> m_ctrl;
};

#endif // ADDUSERDIALOG_H
