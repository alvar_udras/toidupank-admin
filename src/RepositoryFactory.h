#pragma once
#include "PiirkondRepository.h"
#include "UserRepository.h"
#include "UserDummyRepository.h"
#include "PiirkondDummyRepository.h"

class IRepositoryFactory
{
public:
    virtual QSharedPointer<IPiirkondRepository> getPiirkondRepository() = 0;
    virtual QSharedPointer<IUserRepository> getUserRepository() = 0;
};

class DummyRepositoryFactory : public IRepositoryFactory
{
public:
    QSharedPointer<IPiirkondRepository> getPiirkondRepository() override
    {
        return QSharedPointer<IPiirkondRepository>(new PiirkondDummyRepository());
    }
    QSharedPointer<IUserRepository> getUserRepository() override
    {
        return QSharedPointer<IUserRepository>(new UserDummyRepository());
    }
};

class DBRepositoryFactory : public IRepositoryFactory
{
public:
    QSharedPointer<IPiirkondRepository> getPiirkondRepository() override
	{
        return QSharedPointer<IPiirkondRepository>( new PiirkondDbRepository() );
	}
    QSharedPointer<IUserRepository> getUserRepository() override
    {
        return QSharedPointer<IUserRepository>( new UserDbRepository() );
    }
};
