#include "userlistmodel.h"

UserListModel::UserListModel(QList<User> users, QObject *parent) :
      m_users( users )
    , QAbstractListModel(parent)
{
}

int UserListModel::rowCount(const QModelIndex &parent) const
{
    return m_users.count();
}

QVariant UserListModel::data(const QModelIndex &index, int role) const
{
    if ( role == Qt::DisplayRole )
        return m_users[index.row()].kasutajanimi;
    return QVariant();
}

User UserListModel::QModelIndexToUser(const QModelIndex &index)
{
    return m_users[index.row()];
}
