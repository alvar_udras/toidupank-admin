#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QAction>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupMenu();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupMenu()
{
    QMenuBar* menuBar =  new QMenuBar( this );
    QMenu* tegevusedMenu = new QMenu( "Tegevused", menuBar );
    menuBar->addMenu( tegevusedMenu );

    QAction* addUserAction = new QAction("Lisa kasutaja", tegevusedMenu);
    connect(addUserAction, SIGNAL(triggered()), this, SIGNAL(addUserRequested()) );

    QAction* displayUsersAction = new QAction( "Vaata kasutajaid",tegevusedMenu );
    connect(displayUsersAction , SIGNAL(triggered()), this, SIGNAL(showUsersRequested()) );

    QAction* addUserPrefilledAction = new QAction( "Vaata eeltäidetud kasutajat", tegevusedMenu);
    connect(addUserPrefilledAction, SIGNAL(triggered()), this, SIGNAL(addUserPrefilledRequested()));

    tegevusedMenu->addAction( addUserAction );
    tegevusedMenu->addAction(displayUsersAction);
    tegevusedMenu->addAction(addUserPrefilledAction);
}
