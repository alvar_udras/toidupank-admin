#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "RepositoryFactory.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setupMenu();

signals:
    void addUserRequested();
    void showUsersRequested();
    void addUserPrefilledRequested();

private slots:
    
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
