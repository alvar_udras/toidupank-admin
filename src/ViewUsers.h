#ifndef VIEWUSERS_H
#define VIEWUSERS_H

#include <QDialog>
#include <QAbstractListModel>
#include "RepositoryFactory.h"
#include "UserListModel.h"
#include "PiirkondListModel.h"

namespace Ui {
class ViewUsers;
}

class ViewUsers : public QDialog
{
    Q_OBJECT

public:
    explicit ViewUsers(IRepositoryFactory& repoFactory, QWidget *parent = 0);
    ~ViewUsers();
    void setUserListModel( QAbstractListModel* userListModel );
    void setPiirkondListModel ( QAbstractListModel* piirkondListModel );

    void showModalDialog();
signals:
    void changeUserPressedSignal( User u );
private:
    Ui::ViewUsers *ui;
    QScopedPointer<UserListModel> m_userListModel;
    QScopedPointer<PiirkondListModel> m_piirkndListModel;
    IRepositoryFactory& m_repoFactory;
private slots:
    void userSelected( const QModelIndex& index);
    void editUserPressed();
};

#endif // VIEWUSERS_H
